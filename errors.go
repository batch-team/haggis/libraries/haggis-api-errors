package errors

import "github.com/pkg/errors"

//This package pretty much wraps an extended error package in order to provide custom errors specific to haggis.

//New pretty much overrides and wraps the standard errors.New() function in order to avoid conflicts. Not sure if needed.
func New(message string) error {
	return errors.New(message)
}

//NotFound represents a not found error
type NotFound interface {
	NotFound() bool
}

//NewNotFound returns an error that satisfies the NotFound interface
func NewNotFound(message string) error {
	return &notFound{New(message)}
}

type notFound struct {
	error
}

func (nf notFound) NotFound() bool {
	return true
}

//EmtpyMap represents an empty map error
type EmtpyMap interface {
	EmtpyMap() bool
}

//NewEmptyMap returns an error that satisfies the EmptyMap interface
func NewEmptyMap(message string) error {
	return &emptyMap{New(message)}
}

type emptyMap struct {
	error
}

func (em emptyMap) EmtpyMap() bool {
	return true
}

//AlreadyExists represents an 'already exists' error
type AlreadyExists interface {
	AlreadyExists() bool
}

//NewAlreadyExists returns an error that satisfies the AlreadyExists interface
func NewAlreadyExists(message string) error {
	return &alreadyExists{New(message)}
}

type alreadyExists struct {
	error
}

func (e alreadyExists) AlreadyExists() bool {
	return true
}

//InvalidObject represents an 'already exists' error
type InvalidObject interface {
	InvalidObject() bool
}

//NewInvalidObject returns an error that satisfies the InvalidObject interface
func NewInvalidObject(message string) error {
	return &invalidObject{New(message)}
}

type invalidObject struct {
	error
}

func (o invalidObject) InvalidObject() bool {
	return true
}

//BadRequest represents a user input error
type BadRequest interface {
	BadRequest() bool
}

//NewBadRequest returns an error that satisfies the InvalidObject interface
func NewBadRequest(message string) error {
	return &badRequest{New(message)}
}

type badRequest struct {
	error
}

func (r badRequest) BadRequest() bool {
	return true
}

//Wrap is a very shallow wrapper for the errors.Wrap() function. Not very pretty but will work for now.
func Wrap(err error, message string) error {
	return errors.Wrap(err, message)
}

func Wrapf(err error, format string, values ...string) error {
	return errors.Wrapf(err, format, values)
}
