module gitlab.cern.ch/batch-team/haggis/libraries/haggis-api-errors

go 1.12

require github.com/pkg/errors v0.9.1
